const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = 3000;
app.use(bodyParser.json());

app.get('/home', (request, response) => {
  response.send('Welcome to homepage');
})

app.get('/users', (request, response) => {
  let data = [
    { 
      username: "johndoe", 
      password: "johndoe1234"
    },
    { 
      username: "johndoe2", 
      password: "johndoe5678"
    },    
  ];
  response.json(data);

});

app.put('/user-update', (request, response) => {
  const user = request.body.username; 
  response.send(`User ${user} has been updated`);
})

//delete method using URL method
app.delete('/delete-user/:username', (request, response) => {
  const user = request.params.username; 
  response.send(`User ${user} has been deleted`);
})


//delete method using request body directly typed on postman
app.delete('/delete-user', (request, response) => {
  response.send(`User ${request.body.username} has been deleted`);
})

app.listen(port, () => {
  console.log(`index.js Listening on port ${port}`)
})